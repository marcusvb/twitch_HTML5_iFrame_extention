# Twitch HTML5 iFrame Extention

I can't stand flash or any company who supports flash. Worse enough, Google Chrome default to Pepperflash which makes my poor Macbook's Chrome window unusable when taxed to heavily.

However, using a forced player URL from Twitch.tv, I can load a non-flash (with HTML5 wrapped) stream.

Thus this extention simply replaces the inner-html contents of the ```player``` div with an ```iframe``` which is the forced URL.

Simple enough, and works on any channel.
